# Plan

## Branching & Merging - Toby

- working with multiple branches
- comparing across branches
- merging locally
- cherry-picking commits
- forks and branches
- Issues, Merge Requests, Milestones, Labels, Protected Branches

## Introduction to Continuous Integration* - Marc

- configuring CI
- automated testing
- build & deploy

## Introduction to GitLab Pages* - Marc

- configuring Pages
- templates

* material for these topics could be based on the content of [horsing-around][horsing-around].

[horsing-around]: https://git.embl.de/stamper/horsing-around/