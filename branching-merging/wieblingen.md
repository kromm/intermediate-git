# Wieblingen

### Flammkuchenhof

Pariser Weg 1
69123 Heidelberg

https://www.flammkuchenhof.de/

#### Pro

- great Flammkuchen, freshly prepared
- "all you can eat" possible
- great for groups 

#### Con

- a bit far off from the center
